#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Set the value of size to be the number of valid entries in the array (not the total array length).
char ** loadDictionary(char *filename, int *size)
{
	FILE *in = fopen(filename, "r");
	if (!in)
	{
	    fprintf(stderr, "Can't open %s for reading: ", filename);
        perror(NULL);
        exit(1);
	}
	
    // Allocate memory for an array of strings
    int capacity = 10;
    char **words = malloc(capacity * sizeof(char *));

    // Read the dictionary line by line
    char line[100];
    int sizeInc = 0;
    while (fgets(line, 100, in) != NULL)
    {
        // Trim newline
        char *nl = strchr(line, '\n');
        if (nl) *nl = '\0';
        
        // Allocate space for word, copy from line
        char *word = malloc(strlen(line) + 1);
        strcpy(word, line);
        
        // Attach string to larger array
        words[sizeInc] = word;
        sizeInc++;
        
        // Expand words array when capacity is reached
        if (sizeInc == capacity)
        {
            // Make array bigger
            capacity += 10;
            words = realloc(words, capacity * sizeof(char *));
        }
    }
	
	// The size should be the number of entries in the array
    *size = sizeInc;
    
    // Return pointer to the array of strings
    return words;
}
// Search the dictionary for the target string.
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size)
{
    if (dictionary == NULL) return NULL;
    
	for (int i = 0; i < size; i++)
	{
	    if (strcmp(target, dictionary[i]) == 0)
	    {
	        return dictionary[i];
	    }
	}
	return NULL;
}